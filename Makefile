Repl: Repl.hs Parser.hs Eval.hs Lexer.hs 
	ghc --make Repl

repl: Repl

Parser.hs: Parser.y
	happy Parser.y

clean:
	rm Repl
	rm Parser.hs
	rm *.o
	rm *.hi
