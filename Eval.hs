module Eval where
import Lexer
import Parser
import Control.Monad
import Data.Fixed (mod')
import Data.Maybe (fromMaybe)
import Data.Maybe
import Data.List
{-

data Expr = IntExp Int | RealExp Float | BoolExp Bool | VarExp Var  
            | LetInExp Var Expr Expr
            | NegExp Expr | OpExp BinOp Expr Expr 
            | NotExp Expr
            | IfExp Expr Expr Expr 
            | StringExp String | ConstExp Const
            deriving (Show, Eq)

-}

type Env = [(Var, Value)]
type Loc = Int
type Store = [(Loc, Value)]
data Context = Context {env :: Env, store :: Store, table :: CTable}

data OElem = Method Expr Env | Field Int deriving (Eq, Show)
type OTable = [(Name, OElem)]
type CTable = [(Name, (Maybe Name, OTable))]


updateMap :: Eq a => a -> b -> [(a,b)] -> [(a,b)]
updateMap key val map = (key,val) : map'
     where map' = filter notMatch map
           notMatch (a,_) = not (a == key)

freshLoc :: Context -> Loc
freshLoc cont = if (store cont) == [] then 1
                 else let max = maximum (map fst (store cont))
                      in max + 1

--getLoc :: Var -> Env -> Loc
--getLoc x cont = fromJust (lookup x cont)

getVal :: Loc -> Store -> Value
getVal l cont = fromJust (lookup l cont)

addVal :: Value -> Context -> (Loc, Store)
addVal v cont =
    let l = freshLoc cont
        st' = updateMap l v (store cont)
    in (l, st')

updateEnv :: Var -> Value -> Env -> Env
updateEnv key val [] = [(key , val)]
updateEnv key val ((k,_):xs) | key == k = (k, val):xs
updateEnv key val ((k,v):xs) = (k,v):(updateEnv key val xs)
                          

data Value = IntVal Int | RealVal Float | BoolVal Bool | StringVal String | String 
            | PairVal Value Value | FunVal Var Expr Env | UnitVal | RefVal Loc | Object (Name,[Value],Int) | NameVal Name | StoreVal Store

instance Show Context where
    show cont = show (store cont, env cont, table cont)
instance Show Value where
    show (IntVal x) = show x
    show (RealVal y) = show y
    show (StoreVal y) = show y
    show (Object x) = show x
    show (NameVal x) = show x
    show (BoolVal True) = "true"
    show (BoolVal False) = "false"
    show (UnitVal) = "[]"
    show (FunVal _ _ _) = "function"
    --show (PrimFun f) = "built-in function"
    show (StringVal s) = concat ["\"",s,"\""]
    show (RefVal x) = let a = "StorLoc: "
                          b = show x
                      in concat [a,b] 
    show (PairVal v1 v2) = let v1s = show v1
                               v2s = show v2
                           in concat ["[",v1s,",",v2s,"]"]

instance Eq Value where
    v1 == v2 = case (v1, v2) of
        (IntVal a, IntVal b)           -> a == b
        (BoolVal a, BoolVal b)         -> a == b
        (StringVal a, StringVal b)     -> a == b
        (PairVal a1 a2, PairVal b1 b2) -> (a1 == b1) && (a2 == b2)
        (UnitVal, UnitVal)             -> True
        _ -> case (toFloat v1, toFloat v2) of
                (Just a, Just b) -> a == b
                _ -> False

appMaybe :: (a -> Maybe b) -> Maybe a -> Maybe b
appMaybe f a = do av <- a
                  f av

appMaybe2 :: (a -> b -> Maybe c) -> Maybe a -> Maybe b -> Maybe c
appMaybe2 f a b = do
                    av <- a
                    bv <- b
                    f av bv

threadStore :: (Value -> Maybe Value) -> Context -> Expr -> Maybe (Value, Store)
threadStore f cont exp = do
			  (v,s) <- evalE cont exp
                          fval <- f v
                          Just (fval, s)

threadStore2 :: (Value -> Value -> Maybe Value) -> Context -> Expr -> Expr -> Maybe (Value, Store)
threadStore2 f cont a b = do 
                            (va,sa) <- evalE cont a
                            (vb,sb) <- evalE cont b
                            fval <- f va vb
                            Just (fval, sb)



toFloat :: Value -> Maybe Float
toFloat (RealVal y) = Just y
toFloat (IntVal x) = Just (fromIntegral x)
toFloat _ = Nothing

divideV :: Value -> Value -> Maybe Value
divideV a b = 
    case (toFloat a, toFloat b) of
        (Just a, Just b) -> Just (RealVal (a / b))
        _ -> Nothing

arithOp :: (Int -> Int -> Int) -> (Float -> Float -> Float) -> Value -> Value -> Maybe Value
arithOp iop _ (IntVal a) (IntVal b) = (Just . IntVal) (iop a b)
arithOp _ rop a b =
    case (toFloat a, toFloat b) of
        (Just a, Just b) -> (Just . RealVal) (rop a b)
        _ -> Nothing

cmpOp :: (Int -> Int -> Bool ) -> (Float -> Float -> Bool ) -> Value -> Value -> Maybe Value
cmpOp icmp _ (IntVal a) (IntVal b) = (Just . BoolVal) (icmp a b)
cmpOp _ rcmp a b =
    case (toFloat a, toFloat b) of
        (Just a, Just b) -> (Just . BoolVal) (rcmp a b)
        _ -> Nothing

relOp :: (Bool -> Bool -> Bool) -> Value -> Value -> Maybe Value
relOp cmp (BoolVal a) (BoolVal b) = (Just . BoolVal) (cmp a b)
relOp _ _ _ = Nothing

eqV, neqV :: Value -> Value -> Maybe Value
eqV v1 v2  = Just $ BoolVal (v1 == v2)
neqV v1 v2 = Just $ BoolVal (v1 /= v2)

negateV :: Value -> Maybe Value
negateV (IntVal a) = Just $ IntVal (-a)
negateV (RealVal a) = Just $ RealVal (-a)
negateV _ = Nothing

notV :: Value -> Maybe Value
notV (BoolVal b) = Just $ BoolVal (not b)
notV _ = Nothing

floatOfConst :: Const -> Float
floatOfConst PiConst  = pi
floatOfConst PhiConst = (1+sqrt(5))/2
floatOfConst TauConst = 2*pi
floatOfConst EConst   = exp 1

binOpV :: BinOp -> Maybe Value -> Maybe Value -> Maybe Value
binOpV PlusOp = appMaybe2 $ arithOp (+) (+)
binOpV MultOp = appMaybe2 $ arithOp (*) (*)
binOpV MinusOp = appMaybe2 $ arithOp (-) (-)
binOpV DivOp = appMaybe2 divideV
binOpV ExpOp = appMaybe2 $ arithOp (^) (**)
binOpV ModOp = appMaybe2 $ arithOp mod (mod')
binOpV LtOp = appMaybe2 $ cmpOp (<) (<)
binOpV GtOp = appMaybe2 $ cmpOp (>) (>)
binOpV LteOp = appMaybe2 $ cmpOp (<=) (<=)
binOpV GteOp = appMaybe2 $ cmpOp (>=) (>=)
binOpV EqOp = appMaybe2 $ eqV
binOpV NeqOp = appMaybe2 $ neqV
binOpV AndOp = andV
binOpV OrOp = orV

andV, orV :: Maybe Value -> Maybe Value -> Maybe Value
andV (Just (BoolVal False)) _ = Just (BoolVal False)
andV a b = appMaybe2 (relOp (&&)) a b
orV (Just (BoolVal True)) _ = Just (BoolVal True)
orV a b = appMaybe2 (relOp (&&)) a b

evalE :: Context -> Expr -> Maybe (Value, Store)
evalE env (IntExp n) = Just $ (IntVal n, (store env))
evalE env (RealExp r) = Just $ (RealVal r, (store env))
evalE env (BoolExp b) = Just $ (BoolVal b, (store env))
evalE env (ConstExp c) = Just $ ((RealVal $ floatOfConst c), (store env))
evalE env (StringExp s) = Just $ (StringVal s, (store env))
evalE cont (VarExp a) = do
                         val <- lookup a (env cont)
                         Just (val, (store cont)) 
evalE cont (LetInExp var a b) = do (av, s) <- evalE cont a
                                   let env' = updateEnv var av (env cont)
                                   evalE (Context {env=env', store = s, table = (table cont)}) b
evalE env (NegExp a) = threadStore negateV env a 
evalE env (NotExp a) = threadStore notV env a
evalE cont (OpExp op a b) = do 
                            (va,sa) <- evalE cont a
                            (vb,sb) <- evalE (Context {env=(env cont), store = sa, table = (table cont)}) b
                            val <- (binOpV op) (Just va) (Just vb)
                            Just (val,sb) 
--(binOpV op) (evalE env a) (evalE env b)
evalE cont (IfExp a b c) = 
        case evalE cont a of
            Just (BoolVal val, s) -> evalE (Context {env=(env cont), store = s, table = (table cont)}) (if val then b else c)
            _ -> Nothing
evalE cont (PairExp a b) = do
                           (va,sa) <- evalE cont a
                           (vb,sb) <- evalE (Context {env=(env cont), store = sa, table = (table cont)}) b
                           val <- liftM2 PairVal (Just va)  (Just vb)
                           Just (val, sb) 
--liftM2 PairVal (evalE env a) (evalE env b)
evalE env UnitExp = Just (UnitVal,(store env))
evalE cont (FunExp var e) = Just $ (FunVal var e (env cont), (store cont))
evalE env (AppExp a b) =
        case (evalE env a, evalE env b) of
            (Just ((FunVal x exp fenv),sa), Just (val2,sb)) ->
                let fenv' = updateEnv x val2 fenv
                in evalE (Context {env=fenv',store = sb, table = (table env)}) exp
            _ -> Nothing
--evalE env (FstExp e) = threadStore fstV env e
--evalE env (SndExp e) = threadStore sndV env e
{-evalE cont (FstExp e) = do
                           (v,s) <- evalE cont e
                           val <- appMaybe fstV (Just v)
                           Just (val,s)
evalE cont (SndExp e) = do
                           (v,s) <- evalE cont e
                           val <- appMaybe sndV (Just v)
                           Just (val,s) 
-}
evalE cont (FstExp f) = do
		          (v,s) <- evalE cont f
                          val <- fstV v
			  Just (val,s)
evalE cont (SndExp s) = do
                          (v,s) <- evalE cont s
                          val <- sndV v
			  Just (val,s)
evalE cont (RefExp e) = do (v,s) <- evalE cont e
                           let (l, st') = addVal v cont
                           Just ((RefVal l),st') 
evalE cont (DeRefExp e) = do (RefVal a,s) <- evalE cont e
                             val <- lookup a s
                             Just (val, s)                       
evalE cont (AssignExp a b) = do (RefVal c, sone) <- evalE cont a  
                                (v,sb) <- evalE (Context {env = (env cont), store = sone, table = (table cont)}) b
                                let store' = (c,v):[(l,v)|(l,v)<-sb,l /= c] 
                                Just (UnitVal, store')
evalE cont (SeqExp a b) = do (v,s) <- evalE cont a
                             (v',s') <- evalE (Context {env=(env cont), store = s, table = (table cont)}) b
                             Just (v',s')                              
evalE cont (WhoopExp a b) = case evalE cont a of
                               (Just ((BoolVal False),s)) -> Just(UnitVal,s)
                               (Just ((BoolVal True),s)) -> 
                                     do (vb,sb) <- evalE cont b
                                        evalE (Context {env=(env cont), store = sb, table = (table cont)}) (WhoopExp a b)


evalE cont (NewExp n el) = do let ls = (length(store cont))+1
                              (vals, stores) <- threadStoreLst cont el []
                              let llength = length(vals)
                              flength <- getLength cont n 0 0 
                              if (llength == flength) then Just (Object (n,vals,ls), stores) else Nothing

  

--Just (NameVal "test",(store cont))

evalE cont (CallExp e n) =  do (Object (na,vals,ls),stored) <- evalE cont e
                               --let objecttabletup = [(Just a,b)|(a,b)<-(table cont), a==na]
                               --parent <- fst(objecttabletup!!0)
                               (olem, count) <- lookupElem cont na n 0 0
                               let env' = updateEnv "Self" (Object (na,vals,ls)) (env cont)
                               case olem of
                                    (Method exprs envs) -> evalE (Context {env=env', store = stored, table = (table cont)}) exprs--(AppExp exprs (VarExp "Fun")) 
                                    (Field inty) -> Just (vals!!(inty+count),stored)--Just (RefVal (inty+ls),stored)--Just (fromJust (lookup (inty+1) stored), stored



--)--(getVal (inty) stored,stored)--Just (([b|(a,b) <- stored, a==inty]),stored)          --Just (StoreVal stored, stored)--Just (fromJust(lookup 0 stored), stored)                             
getLength cont cname count test = if (test > 1000) then Just count
                                         else do let classtable = (table cont)
                                                 let objecttabletup = [(Just a,b)|(a,b)<-classtable, a==cname]
                                                 if (length(objecttabletup) == 0) then Nothing
                                                 else do let parent = fst(snd(objecttabletup!!0))
                                                         let objecttablep = snd(snd(objecttabletup!!0))
                                                         let newcount = length([Field a|(cname, Field a)<-objecttablep])
                                                         case parent of
                                                              (Just a) -> getLength cont (fromJust(parent)) (count+newcount) (test+1) 
                                                              _        -> Just (count+newcount)                                                       
 
                                                        -- let rettup = [(c,d)|(c,d)<-objecttablep, c==ename]
                                                        -- if (rettup == []) then case parent of
                                                        --                        (Just a) -> lookupElem cont (fromJust(parent)) ename (count+newcount) (test+1)
                                                        --                        _ -> Nothing
                                                        --                   else let ret = snd(rettup!!0) in Just (ret,count)







--getLength :: Name -> Int -> Int -> Context -> Int
--getLength cname acc count cont = if (count==1000) then acc
--                                                  else do let objecttabletup = [(Just a,b)|(a,b)<-(table cont), a == cname]
--                                                          let objecttablep = snd(snd(objecttabletup!!0))
--                                                          fun <- [Field a|(cname, Field a)<-objecttablep]
--                                                          newcount <- length(fun)
--                                                          parent <- fst(snd(objecttabletup!!0))
--                                                          getLength parent (count+newcount) (count+1) cont

 
threadStoreLst cont (x:xs) acc  = do (v,s) <- evalE cont x                                     
                                     threadStoreLst (Context {env=(env cont), store = s, table = (table cont)}) xs (v:acc)
threadStoreLst cont [] acc = Just ((reverse acc),(store cont))


evalS :: Context -> Stmt -> Maybe (Context, Value)
evalS cont (ExpS e) = do (val, s) <- evalE cont e
                         Just ((Context {env=(env cont),store = s, table = (table cont)}), val)
evalS cont (LetS var e) =  do (val,s) <- evalE cont e
                              let env' = updateEnv var val (env cont)
                              Just ((Context {env=env',store = s, table = (table cont)}), UnitVal)
evalS cont (LetRecS var e) =
        let env' = updateEnv var val (env cont)
            val  = fromMaybe UnitVal (Just (fst(fromJust(evalE (Context {env=env',store = (store cont), table = (table cont)}) e)))) 
        in Just ((Context {env=env',store = (store cont), table = (table cont)}), UnitVal)


--evalR :: Context -> Stmt -> CTable
--evalS :: Context -> Stmt -> Maybe (Context,Value)
evalS cont (SpecS name nl ml) = let fields = [(a,Field ((elemIndices a nl)!!0))|a <- nl]
                                    methods = [(name2,(Method e (env cont)))|(Attack name2 e) <- ml]                                    
                                    otable = fields++methods 
                                    funtable = (table cont)
                                    thetable = [(name,(Nothing,otable))]++funtable
                                in Just ((Context {env=(env cont), store = (store cont), table=(thetable)}), UnitVal)

--type NList = [Name]
--type MList = [MDef]
--type EList = [Expr]
-- SpecES Name Name NList MList deriving
--species name evolves name '{' traits NList ';' MList '}'    { SpecES $2 $4 $7 $9 }                                  



evalS cont (SpecES name origs nl ml) = let fields = [(a,Field ((elemIndices a nl)!!0))|a <- nl]
                                           methods = [(name2,(Method e (env cont)))|(Attack name2 e) <- ml]
                                           otable = fields++methods
                                           funtable = (table cont)
                                           thetable = [(name,(Just origs,otable))]++funtable
                                       in Just ((Context {env=(env cont), store = (store cont), table=(thetable)}), UnitVal)


	
index :: Int -> [a] -> Maybe a
index a xs = if (a < (length(xs) + 1)) then Just (xs!!(a-1)) else Nothing 

lookupElem :: Context -> Name -> Name -> Int -> Int -> Maybe (OElem, Int)
lookupElem cont cname ename count test = if (test > 1000) then Nothing 
                                         else do let classtable = (table cont)
                                                 let objecttabletup = [(Just a,b)|(a,b)<-classtable, a==cname]
                                                 if (length(objecttabletup) == 0) then Nothing 
                                                 else do let parent = fst(snd(objecttabletup!!0))
		                                         let objecttablep = snd(snd(objecttabletup!!0))
                                                         let newcount = length([Field a|(cname, Field a)<-objecttablep])
                                                         let rettup = [(c,d)|(c,d)<-objecttablep, c==ename]                                       
                                                         if (rettup == []) then case parent of
                                                                                (Just a) -> lookupElem cont (fromJust(parent)) ename (count+newcount) (test+1)
                                                                                _ -> Nothing                                              
                                                                           else let ret = snd(rettup!!0) in Just (ret,count)
				  
				 
fstV, sndV :: Value -> Maybe Value
fstV value = 
    case value of
    (PairVal v1 v2) -> Just v1
    _ -> Nothing

sndV value = 
    case value of
    (PairVal v1 v2) -> Just v2
    _ -> Nothing

fstFun, sndFun :: Value
fstFun = FunVal "X" (FstExp (VarExp "X"))  []
sndFun = FunVal "X" (SndExp (VarExp "X"))  []

baseEnv :: Env
baseEnv = [("Fst", fstFun), ("Snd", sndFun)]
