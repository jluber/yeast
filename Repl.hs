module Main where
import Lexer
import Parser
import Eval
import System.IO
import Control.Monad


getBlock :: IO String
getBlock =
    let aux acc = do
        line <- getLine
        let acc' = acc++" "++line
            toks = lexer acc'
            ast = appMaybe parse toks
        case (line, ast) of
            ("", _)      -> return acc'
            (_, Nothing) -> aux acc'
            _            -> return acc'
        in aux ""

repl :: Context -> IO ()
repl env = do
    end <- isEOF 
    if end 
    then return () 
    else do
        line <- getBlock
        let toks    = lexer line
            ast     = appMaybe parse toks
            result  = appMaybe (evalS env) ast
        case (toks, ast, result) of 
             (Just [], _, _) -> do repl env
             (Nothing, _, _) -> do 
                 putStrLn $ "Invalid syntax: " ++ line
                 repl env 
             (_, Nothing, _) -> do
                 putStrLn $ "Invalid expression: " ++ line
                 repl env 
             (_, _ , Nothing) -> do
                 putStrLn $ "Evaluation error: " ++ line
                 repl env 
             (_, _, Just (env', UnitVal)) -> do
                 repl env'
             (_, _, Just (env', val)) -> do
                 print val
                 repl env'

baseContext :: Context
baseContext = Context {env = [], store = [], table = []}

main = repl baseContext
