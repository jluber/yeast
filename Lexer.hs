module Lexer where
import Data.Char

type Var = String

data Token =  IntTok Int | RealTok Float | BoolTok Bool | VarTok Var
            | PlusTok | MinusTok | DivTok | MultTok | LPTok | RPTok | EqTok | NeqTok | LtTok | GtTok
            | LteTok | GteTok | DevourTok | NomTok | StringTok String | IfTok | ThenTok | ElseTok  
            | LetTok | InTok | AndTok | OrTok | NotTok
            | ExpTok | ModTok | PiTok | PhiTok | ETok | TauTok
            | FstTok | SndTok | WhoopTok | DoTok | DoneTok 
            | LambTok | ArrowTok | RecTok | LBTok | RBTok | CommaTok | RefTok | BangTok | AssignTok | SeqTok
            | LeftCBTok | RightCBTok | PeriodTok | EvolvesTok | AttackTok | SpeciesTok | NameTok String | NewTok | TraitsTok | SelfTok
            deriving Show

lexer :: String -> Maybe [Token]
lexer str = 
    let auxLex str acc = 
          case str of
            '-':'>':xs -> auxLex xs (ArrowTok:acc)
            ',':xs -> auxLex xs (CommaTok:acc)
            '[':xs -> auxLex xs (LBTok:acc)
            ']':xs -> auxLex xs (RBTok:acc)
            '-':xs -> auxLex xs (MinusTok:acc)
            ';':xs -> auxLex xs (SeqTok:acc)
            '+':xs -> auxLex xs (PlusTok:acc)
            '*':xs -> auxLex xs (MultTok:acc)
            '/':xs -> auxLex xs (DivTok:acc)
            '%':xs -> auxLex xs (ModTok:acc)
            '^':xs -> auxLex xs (ExpTok:acc)
            '(':xs -> auxLex xs (LPTok :acc)
            ')':xs -> auxLex xs (RPTok :acc)
            ':':'=':xs -> auxLex xs (AssignTok:acc)
            '<':'=':xs -> auxLex xs (LteTok:acc)
            '>':'=':xs -> auxLex xs (GteTok:acc)
            '<':xs -> auxLex xs (LtTok:acc)
            '>':xs -> auxLex xs (GtTok:acc)
            '.':xs -> auxLex xs (PeriodTok:acc)
            '{':xs -> auxLex xs (LeftCBTok:acc)
            '}':xs -> auxLex xs (RightCBTok:acc)
            '=':xs -> auxLex xs (EqTok:acc)
            '!':'=':xs -> auxLex xs (NeqTok:acc)
            '!':xs -> auxLex xs (BangTok:acc)
            '"':xs -> lexString xs acc
            x:xs | isSpace x -> auxLex xs acc
            x:_ | isDigit x -> lexInt str [] acc
            x:_ | isAlpha x -> lexKeyword str acc
            [] -> Just (reverse acc)
            _ -> Nothing
        lexInt str acc1 acc2 = 
          case str of
            '.':xs -> lexReal xs ('.':acc1) acc2
            x:xs | isDigit x -> lexInt xs (x:acc1) acc2
            _ -> auxLex str ((IntTok . read . reverse) acc1:acc2)
        lexReal str acc1 acc2 =
          case str of
            '.':_ -> Nothing
            x:xs | isDigit x -> lexReal xs (x:acc1) acc2
            _ -> auxLex str ((RealTok . read . reverse) acc1:acc2)
        lexKeyword str acc = 
          case span isVarChar str of
            ("if", xs) -> auxLex xs (IfTok:acc)
            ("then", xs) -> auxLex xs (ThenTok:acc)
            ("else", xs) -> auxLex xs (ElseTok:acc)
            ("true", xs) -> auxLex xs (BoolTok True:acc)
            ("false", xs) -> auxLex xs (BoolTok False:acc)
            ("let", xs) -> auxLex xs (LetTok:acc)
            ("in", xs) -> auxLex xs (InTok:acc)
            ("Fst", xs) -> auxLex xs (FstTok:acc)
            ("self", xs) -> auxLex xs (SelfTok:acc)
            ("species", xs) -> auxLex xs (SpeciesTok:acc)
            ("evolves", xs) -> auxLex xs (EvolvesTok:acc)
            ("attack", xs) -> auxLex xs (AttackTok:acc)
            ("Snd", xs) -> auxLex xs (SndTok:acc)
            ("devour", xs) -> auxLex xs (DevourTok:acc)
            ("lamb", xs) -> auxLex xs (LambTok:acc)
            ("rec", xs) -> auxLex xs (RecTok:acc)
            ("ref", xs) -> auxLex xs (RefTok:acc)
            ("nom", xs) -> auxLex xs (NomTok:acc)
            ("and", xs) -> auxLex xs (AndTok:acc)
            ("new", xs) -> auxLex xs (NewTok:acc)
            ("traits", xs) -> auxLex xs (TraitsTok:acc)
            ("whoop", xs) -> auxLex xs (WhoopTok:acc)
            ("do", xs) -> auxLex xs (DoTok:acc)
            ("done", xs) -> auxLex xs (DoneTok:acc)
            ("or", xs) -> auxLex xs (OrTok:acc)
            ("not", xs) -> auxLex xs (NotTok:acc)
            ("e", xs) -> auxLex xs (ETok:acc)
            ("pi", xs) -> auxLex xs (PiTok:acc)
            ("phi", xs) -> auxLex xs (PhiTok:acc)
            ("tau", xs) -> auxLex xs (TauTok:acc)
            (v:var, xs) -> if isUpper v then auxLex xs (VarTok (v:var):acc) else 
                              if isLower v then auxLex xs (NameTok (v:var):acc) else Nothing
            --(n:name, xs) -> if isLower n then auxLex xs (NameTok (n:name):acc) else Nothing 
            --(n:name, xs) -> if (isLower n && isAlpha n && isValidName name) 
            --                then auxLex xs (NameTok (n:name):acc)
            --                else Nothing
	lexString str acc =
          case break (\ x -> x == '"') str of
            (str, '"':xs) -> auxLex xs (StringTok str:acc)
            _ -> Nothing
    in auxLex str []

isValidName :: String -> Bool
isValidName [] = True
isValidName [x] = isAlphaNum x
isValidName (x:xs) = if isAlphaNum x then isValidName xs else False
        
isVarChar :: Char -> Bool
isVarChar x = isAlphaNum x || x == '\'' || x == '_'
