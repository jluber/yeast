{
module Parser where
import Lexer

type Name = String
type NList = [Name]
type MList = [MDef]
type EList = [Expr]

data Stmt = ExpS Expr | LetS Var Expr | LetRecS Var Expr | SpecS Name NList MList | SpecES Name Name NList MList deriving (Show, Eq)

--data NList = EpsNList | SinNList Name | FulNList Name NList
--data NList = Names NameList deriving (Show, Eq)

--data MList = EpsMList | SinMList MDef | FulMList MDef MList
--data MList = Methods MDefList deriving (Show, Eq)

data MDef = Attack Name Expr deriving (Show, Eq)

--data Elem = ElemNew Name EList | ElemApp Elem Name

--data EList = EpsEList | SinEList Elem | FulEList Elem EList
--data EList = Exprs ExprList deriving (Show, Eq)

data BinOp = PlusOp | MinusOp | DivOp | MultOp | ExpOp | ModOp
           | LtOp | GtOp | EqOp | NeqOp | LteOp | GteOp | AndOp | OrOp
            deriving (Show, Eq)

data Const = PiConst | PhiConst | TauConst | EConst deriving (Show, Eq)

data Expr = IntExp Int | RealExp Float | BoolExp Bool | VarExp Var
            | StringExp String | ConstExp Const
            | LetInExp Var Expr Expr
            | NegExp Expr | OpExp BinOp Expr Expr | NotExp Expr
            | IfExp Expr Expr Expr
            | FunExp Var Expr | AppExp Expr Expr
            | PairExp Expr Expr | UnitExp | WhoopExp Expr Expr
            | FstExp Expr | SndExp Expr | RefExp Expr | DeRefExp Expr
            | AssignExp Expr Expr | SeqExp Expr Expr
            | NewExp Name EList | CallExp Expr Name | SelfExp Var Name 
            deriving (Show, Eq)
}

%monad { Maybe }
%name parse
%tokentype {Token}
%error { parseError}

%token
    int     { IntTok $$ }
    real    { RealTok $$ }
    bool    { BoolTok $$ }
    var     { VarTok $$ }
    and      { AndTok }
    or      { OrTok }
    not      { NotTok }
    if      { IfTok }
    then    { ThenTok }
    let     { LetTok }
    in      { InTok }
    else    { ElseTok }
    '+'     { PlusTok }
    '-'     { MinusTok }
    '/'     { DivTok }
    '*'     { MultTok }
    ';'     { SeqTok }
    '%'     { ModTok }
    '^'     { ExpTok }
    '('     { LPTok }
    ')'     { RPTok }
    '<='    { LteTok }
    '>='    { GteTok }
    '<'     { LtTok }
    '>'     { GtTok }
    '='     { EqTok }
    '!='    { NeqTok }
    ':='    { AssignTok }
    devour  { DevourTok }
    nom     { NomTok }
    str     { StringTok $$}
    e       { ETok }
    pi      { PiTok }
    phi     { PhiTok }
    tau     { TauTok }
    lamb    { LambTok }
    '->'    { ArrowTok }
    ','     { CommaTok }
    '['     { LBTok }
    ']'     { RBTok }
    rec     { RecTok }
    ref     { RefTok }
    '!'     { BangTok }
    'Fst'   { FstTok }
    'Snd'   { SndTok }
    whoop   { WhoopTok }
    do      { DoTok }
    done    { DoneTok }
    species { SpeciesTok }
    '{'     { LeftCBTok }
    '}'     { RightCBTok }
    '.'     { PeriodTok  }
    attack  { AttackTok }
    evolves  { EvolvesTok }
    traits  { TraitsTok }
    name    { NameTok $$ }
    new     { NewTok }
    self    { SelfTok }

--%right '->' LET in
%right '->' LET in species evolves 
%left '}'
%right traits name new MDef
%left if else
%left SOL
%left and or
%left '=' '!='
%left ';'
%nonassoc '<' '>' '<=' '>=' ':='
%left '-' '+'
%left '%'
%left '*' '/'
%left '^'
%left '!' ref
%left NEG
%left ')' '(' 
%left 'Fst' 'Snd'
%right '.'
%%
Stmt : Exp                                { ExpS $1 }
     | let var devour Exp %prec LET       { LetS $2 $4 }
     | let rec var devour Exp             { LetRecS $3 $5 }
     | species name '{' traits NList ';' MList '}'    { SpecS $2 $5 $7 }
     | species name evolves name '{' traits NList ';' MList '}'    { SpecES $2 $4 $7 $9 }

NList : {-empty-}                       { [] }
      | name                          { [$1] }
      | name ','  NList            { $1 : $3 }

MList : {-empty-}                       { [] }
      | MDef MList                 { $1 : $2 }

MDef : attack name '=' Exp     { Attack $2 $4 }

EList : {-empty-}                       { [] }
      | Exp                           { [$1] }
      | Exp ',' EList              { $1 : $3 }

Exp : int                               { IntExp $1 }
    | real                              { RealExp $1 }
    | bool                              { BoolExp $1 }
    | var                               { VarExp $1}
    | str                               { StringExp $1}
    | '-' Exp %prec NEG                 { NegExp $2 }
    | not Exp %prec NEG                 { NotExp $2 }
    | ref Exp                           { RefExp $2 }
    | '!' Exp                           { DeRefExp $2 }
    | Exp '.' name                      { CallExp $1 $3 }
    --| self '.' name                     { SelfExp $1 $3 }
    | new name '(' EList ')'            { NewExp $2 $4 }
    | Exp ';' Exp                       { SeqExp $1 $3 }
    | '(' Exp ')' %prec SOL             { $2 }
    | Exp '^' Exp                       { OpExp ExpOp $1 $3}
    | Exp '/' Exp                       { OpExp DivOp $1 $3 }
    | Exp '*' Exp                       { OpExp MultOp $1 $3 }
    | Exp '%' Exp                       { OpExp ModOp $1 $3}
    | Exp '+' Exp                       { OpExp PlusOp $1 $3 }
    | Exp '-' Exp                       { OpExp MinusOp $1 $3 }
    | Exp ':=' Exp                      { AssignExp $1 $3 }
    | Exp '<' Exp                       { OpExp LtOp $1 $3 }
    | Exp '>' Exp                       { OpExp GtOp $1 $3 }
    | Exp '<=' Exp                      { OpExp LteOp $1 $3 }
    | Exp '>=' Exp                      { OpExp GteOp $1 $3 }
    | Exp '=' Exp                       { OpExp EqOp $1 $3 }
    | self                              { VarExp "Self" }
    | Exp '!=' Exp                      { OpExp NeqOp $1 $3 }
    | Exp and Exp                       { OpExp AndOp $1 $3 }
    | Exp or Exp                        { OpExp OrOp $1 $3 }
    | 'Fst' Exp                         { FstExp $2 }
    | 'Snd' Exp                         { SndExp $2 }
    | whoop Exp do Exp done             { WhoopExp $2 $4 }
    | if Exp then Exp else Exp          { IfExp $2 $4 $6 }
    | let var nom Exp in Exp %prec LET  { LetInExp $2 $4 $6 }
    | Const                             { ConstExp $1}
    | lamb var '->' Exp                 { FunExp $2 $4 }
    | Exp '(' Exp ')'                   { AppExp $1 $3 }
    | '[' ']'                           { UnitExp }
    | '[' Exp ',' Exp ']'               { PairExp $2 $4 }

Const : pi                              { PiConst }
      | phi                             { PhiConst }
      | e                               { EConst }
      | tau                             { TauConst }

{

parseError :: [Token] -> Maybe a
parseError _ = Nothing

}
